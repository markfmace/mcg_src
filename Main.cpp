//////////////////////////////////////////////////////////
// MC-Glauber with Short Range Correlations -- MCG_SRC  //
// v1.0                                                 //
// SRC component by Mark Mace c. 2018                   //
// MC-Glauber adapted from B. Schenke                   //
//////////////////////////////////////////////////////////

#include "mpi.h"
#include <stdio.h>
#include <string>
#include <cmath>
#include <ctime>
#include <iostream>
#include <complex>
#include <fstream>
#include <vector>

#include "Setup.h"
#include "Init.h"
#include "Random.h"
#include "FFT.h"
#include "Parameters.h"
#include "Matrix.h"
#include "Lattice.h"
#include "Spinor.h"
#include "MyEigen.h"

#define _SECURE_SCL 0
#define _HAS_ITERATOR_DEBUGGING 0
using namespace std;

int readInput(Setup *setup, Parameters *param, int argc, char *argv[], int rank);

// main program
int main(int argc, char *argv[]){
    
    int rank;
    int size;
    
    //initialize MPI
    MPI::Init(argc, argv);
    
    rank = MPI::COMM_WORLD.Get_rank(); //number of current processor
    size = MPI::COMM_WORLD.Get_size(); //total number of processors
    
    // welcome
    if(rank==0){
        
        cout << endl;
        cout << "#####################################################" << endl;
        cout << "# MC-Glauber with Short Range Correlations -- MCG_SRC v1.0" << endl;
        cout << "# SRC Code by Mark Mace" << endl;
        cout << "# MC-Glauber based on version by B. Schenke from IPGlasma" << endl;
        cout << "#####################################################" << endl;
        cout << endl;
    }
    
    // initialize helper class objects
    MyEigen *myeigen;
    myeigen = new MyEigen();
    //myeigen->test();
    
    Setup *setup;
    setup = new Setup();
    Random *random;
    random = new Random();
    Parameters *param;
    param = new Parameters();
    
    param->setMPIRank(rank);
    param->setMPISize(size);
    param->setSuccess(0);
    
    // read parameters from file
    readInput(setup, param, argc, argv, rank);
    
    
    
    int cells = param->getSize()*param->getSize();
    int Nc2m1 = param->getNc()*param->getNc()-1; // N_c^2-1
    int nn[2];
    int pos;
    double x,y;
    double ds = param->getDs();
    nn[0]=param->getSize();
    nn[1]=param->getSize();
    
    stringstream strup_name;
    strup_name << "usedParameters" << param->getRandomSeed() << ".dat";
    string up_name;
    up_name = strup_name.str();
    
    //initialize init object
    Init *init;
    init = new Init(nn);
    
    // initialize group
    Group *group;
    group = new Group(param->getNc());
    
    // initialize Glauber class
    Glauber *glauber;
    glauber = new Glauber;
    
    // measure and output eccentricity, triangularity
    //init->eccentricity(lat, group, param, random, glauber);
    
    // initialize Glauber
    glauber->initGlauber(param->getSigmaNN(), param->getTarget(), param->getProjectile(), param->getb(), 100, rank);
    
    // RESET SUCCESS INDICATOR //
    param->setSuccess(0);
    
    while(param->getSuccess()==0)
    {
        // RESET SUCCESS INDICATOR //
        param->setSuccess(0);
        // allocate lattice
        Lattice *lat;
        lat = new Lattice(param, param->getNc(), param->getSize());
        
        //initialize random generator using time and seed from input file
        unsigned long long int rnum;
        
        if(param->getUseSeedList()==0)
        {
            if(param->getUseTimeForSeed()==1)
            {
                rnum=time(0)+param->getSeed()*10000;
            }
            else
            {
                rnum = param->getSeed();
                cout << "# RNGSEED = " << rnum+(rank*1000) << " FROM RANK"  << endl;
            }
            
            param->setRandomSeed(rnum+rank*1000);
            if(param->getUseTimeForSeed()==1)
            {
                cout << "# RNGSEED=" << param->getRandomSeed() << " FROM TIME " << rnum-param->getSeed()-(rank*1000) << " AND ARGUMENT (+1000*rank) " << param->getSeed()+(rank*1000) << endl;
            }
            
            random->init_genrand64(rnum+rank*1000);
        }
        else
        {
            ifstream fin;
            fin.open("seedList");
            unsigned long long int seedList[size];
            if(fin)
            {
                for (int i=0; i<size; i++)
                {
                    if (!fin.eof())
                    {
                        fin >> seedList[i];
                    }
                    else
                    {
                        cerr << "# ERROR -- NOT ENOUGH RANDOM SEEDS" << endl;
                        exit(1);
                    }
                }
            }
            else
            {
                cerr << "# ERROR -- 'seedlist' FILE NOT FOUND" << endl;
                exit(1);
            }
            fin.close();
            //cout << size << " seeds read from file." << endl;
            
            param->setRandomSeed(seedList[rank]);
            random->init_genrand64(seedList[rank]);
            cout << "# RNGSEED ON RANK " << rank << " IS  " << seedList[rank] << " FROM LIST"  << endl;
        }
        
        
        
        ofstream LogOutput(up_name.c_str(),ios::app);
        LogOutput << "# RANK= " << rank << " RNGSEED= " << param->getRandomSeed() << endl;
        LogOutput.close();
        
        // initialize gsl random number generator (used for non-Gaussian distributions)
        random->gslRandomInit(rnum);
        
        // GLAUBER + IP-Sat
        int READFROMFILE = 0;
        if(param->getMode()==1){
            cout << "# MC-Glauber+IP-Sat INIT" << endl;
            init->init(lat, group, param, random, glauber, READFROMFILE);

        }
        else if(param->getMode()==2){
            cout << "# MC-Glauber INIT" << endl;
            init->init_mcg(lat, group, param, random, glauber, READFROMFILE);
        }
        if(param->getMode()==3){
            cout << "# MC-Glauber+SRC INIT" << endl;
            init->init_mcg_src(lat, group, param, random, glauber, READFROMFILE);
        }
        else{
            cerr << "# ERROR -- INCORRECT INIT TYPE -- TYPE SPECIFIED IS=" << param->getMode()<< std::endl;
            exit(0);
        }
        cout << "# INIT FINISHED" << endl;
        
        if(param->getSuccess()==0)
        {
            delete lat;
            continue;
        }
        
        stringstream strNucApos,strNucBpos;
        strNucApos << "NucleusAID" << param->getRandomSeed() << ".txt";
        strNucBpos << "NucleusBID" << param->getRandomSeed() << ".txt";
        
        string NucApos;  NucApos = strNucApos.str();
        string NucBpos;  NucBpos = strNucBpos.str();
        
        ofstream ProjPositions(NucApos.c_str(),ios::out);
        ofstream TargPositions(NucBpos.c_str(),ios::out);
        
        for (int i = 0; i<glauber->nucleusA1(); i++){
            
            ProjPositions << init->nucleusA.at(i).x << " " << init->nucleusA.at(i).y << endl;
        }
        
        for (int i = 0; i<glauber->nucleusA2(); i++){
            
            TargPositions << init->nucleusB.at(i).x << " " << init->nucleusB.at(i).y << endl;
        }
        
        ProjPositions.close();
        TargPositions.close();

        delete init;
        delete random;
        delete glauber;
        
        delete lat;
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    
    delete group;
    delete param;
    delete setup;
    delete myeigen;
    //cout << "done." << endl;
    MPI::Finalize();
    return 1;
}/* main */


int readInput(Setup *setup, Parameters *param, int argc, char *argv[], int rank)
{
    // the first given argument is taken to be the input file name
    // if none is given, that file name is "input"
    //cout << "Opening input file ... " << endl;
    string file_name;
    if (argc>1){
        
        file_name = argv[1];
        if(rank==0)
        cout << "# INPUT FILE " << file_name << endl;
    }
    else{
        
        file_name = "input";
        if(rank==0)
        cout << "# NO INPUT FILE SPECIFIED -- USING " << file_name << endl;
    }
    
    
    // read and set all the parameters in the "param" object of class "Parameters"
    if(rank==0){
        cout << "# READING IN PARAMETERS FROM FILE" << endl;
    }
    param->setNucleusQsTableFileName(setup->StringFind(file_name,"NucleusQsTableFileName"));
    param->setNucleonPositionsFromFile(setup->IFind(file_name,"nucleonPositionsFromFile"));
    param->setTarget(setup->StringFind(file_name,"Target"));
    param->setProjectile(setup->StringFind(file_name,"Projectile"));
    param->setMode(setup->IFind(file_name,"mode"));
    param->setRunningCoupling(setup->IFind(file_name,"runningCoupling"));
    param->setL(setup->DFind(file_name,"L"));
    param->setLOutput(setup->DFind(file_name,"LOutput"));
    param->setBG(setup->DFind(file_name,"BG"));
    param->setBGq(setup->DFind(file_name,"BGq"));
    param->setMuZero(setup->DFind(file_name,"muZero"));
    param->setc(setup->DFind(file_name,"c"));
    param->setSize(setup->IFind(file_name,"size"));
    param->setSizeOutput(setup->IFind(file_name,"sizeOutput"));
    param->setEtaSizeOutput(setup->IFind(file_name,"etaSizeOutput"));
    param->setDetaOutput(setup->DFind(file_name,"detaOutput"));
    param->setUseFluctuatingx(setup->IFind(file_name,"useFluctuatingx"));
    param->setNc(setup->IFind(file_name,"Nc"));
    param->setInverseQsForMaxTime(setup->IFind(file_name,"inverseQsForMaxTime"));
    param->setSeed(setup->ULLIFind(file_name,"seed"));
    param->setUseSeedList(setup->IFind(file_name,"useSeedList"));
    param->setNy(setup->IFind(file_name,"Ny"));
    param->setRoots(setup->DFind(file_name,"roots"));
    param->setNu(setup->DFind(file_name,"tDistNu"));
    param->setUseFatTails(setup->IFind(file_name,"useFatTails"));
    param->setg(setup->DFind(file_name,"g"));
    param->setm(setup->DFind(file_name,"m"));
    param->setJacobianm(setup->DFind(file_name,"Jacobianm"));
    param->setSigmaNN(setup->DFind(file_name,"SigmaNN"));
    param->setRmax(setup->DFind(file_name,"rmax"));
    param->setbmin(setup->DFind(file_name,"bmin"));
    param->setbmax(setup->DFind(file_name,"bmax"));
    param->setQsmuRatio(setup->DFind(file_name,"QsmuRatio"));
    param->setUsePseudoRapidity(setup->DFind(file_name,"usePseudoRapidity"));
    param->setRapidity(setup->DFind(file_name,"Rapidity"));
    param->setUseNucleus(setup->IFind(file_name,"useNucleus"));
    param->setg2mu(setup->DFind(file_name,"g2mu"));
    param->setMaxtime(setup->DFind(file_name,"maxtime"));
    param->setdtau(setup->DFind(file_name,"dtau"));
    // param->setxExponent(setup->DFind(file_name,"xExponent")); //  is now obsolete
    param->setRunWithQs(setup->IFind(file_name,"runWith0Min1Avg2MaxQs"));
    param->setRunWithkt(setup->IFind(file_name,"runWithkt"));
    param->setRunWithLocalQs(setup->IFind(file_name,"runWithLocalQs"));
    param->setRunWithThisFactorTimesQs(setup->DFind(file_name,"runWithThisFactorTimesQs"));
    param->setxFromThisFactorTimesQs(setup->DFind(file_name,"xFromThisFactorTimesQs"));
    param->setLinearb(setup->IFind(file_name,"samplebFromLinearDistribution"));
    param->setWriteOutputs(setup->IFind(file_name,"writeOutputs"));
    param->setWriteEvolution(setup->IFind(file_name,"writeEvolution"));
    param->setAverageOverNuclei(setup->IFind(file_name,"averageOverThisManyNuclei"));
    param->setUseTimeForSeed(setup->IFind(file_name,"useTimeForSeed"));
    param->setUseFixedNpart(setup->IFind(file_name,"useFixedNpart"));
    param->setSmearQs(setup->IFind(file_name,"smearQs"));
    param->setSmearingWidth(setup->DFind(file_name,"smearingWidth"));
    param->setGaussianWounding(setup->IFind(file_name,"gaussianWounding"));
    param->setReadMultFromFile(setup->IFind(file_name,"readMultFromFile"));
    param->setProtonAnisotropy(setup->DFind(file_name,"protonAnisotropy"));
    param->setUseConstituentQuarkProton(setup->DFind(file_name,"useConstituentQuarkProton"));
    if(rank==0){
        cout << "# FINISHED READING PARAMETERS FROM FILE" << endl;
    }
    // write the used parameters into file "usedParameters.dat" as a double check for later
    time_t rawtime = time(0);
    stringstream strup_name;
    strup_name << "usedParameters" << param->getRandomSeed() << ".dat";
    string up_name;
    up_name = strup_name.str();
    
    fstream LogOutput(up_name.c_str(),ios::out);
    char * timestring = ctime(&rawtime);
    LogOutput << "# FILE CREATED ON " << timestring << endl;
    LogOutput << "###################### " << endl;
    LogOutput << "# USED BY MCG_SRC v1.0" << endl;
    LogOutput << "###################### " << endl;
    LogOutput << " " << endl;
    LogOutput << " Output by readInput in main.cpp: " << endl;
    LogOutput << " " << endl;
    LogOutput << "Program run in mode " << param->getMode() << endl;
    LogOutput << "Nc " << param->getNc() << endl;
    LogOutput << "size " << param->getSize() << endl;
    LogOutput << "lattice spacing a " << param->getL()/static_cast<double>(param->getSize()) << " fm " << endl;
    LogOutput << "Ny " << param->getNy() << endl;
    LogOutput << "Projectile " << param->getProjectile() << endl;
    LogOutput << "Target " << param->getTarget() << endl;
    LogOutput << "Gaussian wounding " << param->getGaussianWounding() << endl;
    LogOutput << "Using fluctuating x=Qs/root(s) " << param->getUseFluctuatingx() << endl;
    if( param->getRunWithkt()==0)
    LogOutput << "Using local Qs to run " << param->getRunWithLocalQs() << endl;
    else
    LogOutput << "running alpha_s with k_T" << endl;
    LogOutput << "QsmuRatio " << param->getQsmuRatio() << endl;
    LogOutput << "smeared mu " << param->getSmearQs() << endl;
    LogOutput << "m " << param->getm() << endl;
    LogOutput << "rmax " << param->getRmax() << endl;
    if (param->getSmearQs()==1){
        
        LogOutput << "smearing width " << param->getSmearingWidth() << endl;
    }
    LogOutput << "Using fat tailed distribution " << param->getUseFatTails() << endl;
    LogOutput.close();
    
    return 0;
}
